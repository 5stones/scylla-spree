import json
from scylla import configuration
from scylla import RestClient


configuration.setDefaults('Spree', {
    #'url': None,
    #'token': None,
    'debug': False,
    'verify_ssl': True,
    'version': None
})


class SpreeRestError(RestClient.RestError):
    pass


class SpreeRestClient(RestClient):
    RestError = SpreeRestError

    def __init__(self):
        config = configuration.getSection('Spree')
        if config['version']:
            url = config['url'] + '/api/{0}'.format(config['version'])
        else:
            url = config['url'] + '/api'
        super(SpreeRestClient, self).__init__(url,
            verify_ssl=config['verify_ssl'],
            debug=config['debug'])
        self._set_header('X-Spree-Token', config['token'])
        self.name = config['name']

    def order_search(self, after):
        return self.get('/orders', {
                'q[updated_at_gteq]': after
            })

    def find_or_create(self, record_type, data, q, search_uri='', create_uri='', wrap_keys=None):
        response = self.get(search_uri+'/'+record_type, q)
        if response.get('count') == 0:
            return self.post(create_uri+'/'+record_type, None, data, wrap_keys=wrap_keys)
        return response[record_type][0]

    def find(self, record_type, q, search_uri=''):
        response = self.get(search_uri+'/'+record_type, q)
        if response.get('count') == 0:
            return False
        return response

    def create_product(self, data):
        if self.debug:
            print('Creating Product: '+json.dumps(data, indent=4))
        return self.post('/products', None, data, wrap_keys='product')
        #return client.find_or_create('products', data, {'q[name_cont]': data['name']},
        #        wrap_keys='product' )

    def update_product(self, product_slug, data):
        if self.debug:
            print('Updating Product: '+json.dumps(data, indent=4))
        return self.put('/products/{0}'.format(product_slug), None, data, wrap_keys='product')

    def create_product_property(self, data):
        if self.debug:
            print('Creating Product Property: '+json.dumps(data, indent=4))
        path = '/products/{0}/product_properties?product_property[property_name]={1}&product_property[value]={2}'.format(data['product_id'], data['property_name'], data['value'])
        return self.post(path, None)

    def update_product_property(self, data):
        if self.debug:
            print('Updating Product Property: '+json.dumps(data, indent=4))
        path = '/products/{0}/product_properties/{1}?product_property[value]={2}'.format(data['product_id'], data['property_name'], data['value'])
        return self.put(path, None)

    def upsert_product_property(self, data):
        print('Upserting Product Property: '+json.dumps(data,indent=4))
        path = '/products/{0}/product_properties/{1}'.format(data['product_id'],data['property_name'])
        try:
            if(self.get(path,None)):
                return self.update_product_property(data)
            return self.create_product_property(data)
        except SpreeRestError as e:
            return False

    def removeall_related_products(self, data):
        print('Removing all Related Products from Product: '+json.dumps(data,indent=4))
        path = '/products/{0}/relations/destroy_all'.format(data['product_id'])
        return self.delete(path)

    def add_related_product(self, data):
        print('Adding Related Product: '+json.dumps(data,indent=4))
        path = '/products/{0}/relations/'.format(data['relatable_id'])
        param = {
               "relation_type_id": data['relation_type_id'],
               "relatable_type": data['relatable_type'],
               "related_to_id": data['related_to_id'],
               "related_to_type": data['related_to_type']
        }
        return self.post(path, None, param, wrap_keys='relation')

    def _create_variant(self, product_slug, data):
        if self.debug:
            print('Variant: '+json.dumps(data, indent=4))
        return self.find_or_create('variants', data, {'q[sku_cont]': data['sku']},
                search_uri = '/products/{0}'.format(product_slug),
                create_uri = '/products/{0}'.format(product_slug),
                wrap_keys='variant')

    def update_stock_item(self, variant_id, quantity):
        path = '/stock_locations/1/stock_items/{0}'.format(variant_id)
        data = {}
        data['count_on_hand'] = quantity
        data['force'] = True
        if self.debug:
            print('Updating Stock Item: '+json.dumps(data, indent=4))
        return self.put(path, None, data, wrap_keys='stock_item')

    def get_products(self, page=1, per_page=25):
        get_products_result = self.get('/products?page={0}&per_page={1}'.format(page, per_page))
        total_pages = get_products_result['pages']
        products = []

        products = get_products_result['products']
        for i in range(len(products)):
            product = products[i]
            products.append(product)

        if(page < total_pages):
            products = products + self.get_products(page+1, per_page)

        return products

    def import_product(self, variants, product_conv, variant_conv):

        product_data = product_conv.convert(variants[0])

        if len(variants) == 1:
            # create product with master variant data included
            variant_data = variant_conv.convert(variants[0])
            product_data.update(variant_data)

            # FIXME track inventory is being ignored
            product = self.create_product(product_data)

        else:
            # create product and multiple variants
            # TODO merge categories
            product_data['option_type_ids'] = 1
            product = self.create_product(product_data)

            # determine option_types and option_values
            for variant in variants:
                variant_data = variant_conv.convert(variant)
                variant_data['option_value_ids'] = _get_option_value_ids_for_brand(variant['Brand Name'])
                if self.debug:
                    print('{0}: {1}'.format(variant['Product Name'], variant_data['option_value_ids']))
                self._create_variant(product['slug'], variant_data)

        return product
