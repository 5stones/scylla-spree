"""Core connection to spree and an App to download updates.

configuration required:
    'Spree': {
        'name': "Spree",
        'url': 'https://...',
        'token': '',
        'verify_ssl': None,
        'debug': False,
    },
"""

from .client import SpreeRestClient

from .app import App

from .tasks import ToSpreeTask, SpreeTask, SpreeOrderTask, SpreeStockItemTask, ToSpreeActionTask, CaptureSpreeInvoicesTask, ShipSpreeShipmentsTask, UpdateSpreeStockItemTask

from . import records
