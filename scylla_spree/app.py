from scylla import app
from .client import SpreeRestClient
from .tasks import SpreeTask, SpreeOrderTask, SpreeStockItemTask


class App(app.App):

    main_options = [
        ('after', 'a', None),
        #('limit', 'l', 100),
        ('page', 'p', 1),
        ('retry-errors', 'r'),
    ]

    task_params = [
        ('User',),
        ('Product',),
        ('StockLocation',),
    ]

    on_demand_task_params = [
        ('Country',),
        ('State',),
    ]

    def _prepare(self):
        self.client = SpreeRestClient()

        for task_params in self.task_params:
            task = SpreeTask(self.client, *task_params)
            self.tasks[task_params[0]] = task

        for task_params in self.on_demand_task_params:
            task = SpreeTask(self.client, *task_params)
            self.on_demand_tasks[task_params[0]] = task

        self.tasks['StockItem'] = SpreeStockItemTask(self.client, 'StockItem')
        self.tasks['Order'] = SpreeOrderTask(self.client, 'Order')

    def get_task(self, record_type):
        try:
            return super(App, self).get_task(record_type)
        except KeyError:
            return SpreeTask(self.client, record_type)
