from scylla import tasks
from scylla import orientdb
from scylla import ErrorLogging
from .records import SpreeRecord


class SpreeTask(tasks.Task):
    """Downloads records from Spree.
    """
    api_types = {
        'Country':'countries',
        'State':'states',
        'Order':'orders',
        'User':'users',
        'Product':'products',
        'Bookfair':'bookfairs',
        'Group':'groups',
        'StockLocation':'stock_locations',
        'StockItem': 'stock_locations/{location}/stock_items',
    }

    def __init__(self, client, record_type):
        self.record_type = '{}{}'.format(record_type[0].title(), record_type[1:])

        self.client = client

        self.classname = '{0}{1}'.format(client.name, self.record_type)
        self.api_type = self.api_types[self.record_type]

        task_id = '{0}-download'.format(self.classname)
        super(SpreeTask, self).__init__(task_id)

    def _step(self, after, options):
        args = {}
        if after:
            args['q[updated_at_gteq]'] = after

        print("\nGetting {0} updated after {1} :".format(self.api_type, after))
        self.get_all(args)

    def get_all(self, args={}):
        response = None
        if 'page' not in args or not args['page']:
            args['page'] = 1

        while response is None or args['page'] <= response['pages']:
            response = self.client.get('/{0}'.format(self.api_type), args)
            self._process_search_response(response, self.api_type)
            args['page'] += 1

    def _process_search_response(self, response, key):
        for obj in response[key]:
            if key == 'orders':
                # need to get order details
                self.process_one(obj['number'])
            else:
                self._process_response_record(obj)

    def _process_response_record(self, obj):
        rec = SpreeRecord.factory(self.client, self.record_type, obj)
        with ErrorLogging(self.task_id, rec):
            rec.throw_at_orient()
            self._link_children(rec)
            print("Saved {} {} as {} {}".format(
                self.record_type, obj.get('id'), rec.classname, rec.rid))

    def _link_children(self, rec):
        """Once the record and subrecords are in orientdb, add _parent to children
            and children of children recursively, depth first.
        """
        # pull rids of children
        rids = []
        for subtype in rec._sub_records:
            value = rec.get(subtype)
            if isinstance(value, SpreeRecord):
                # single record
                rids.append(value.rid)
                # recursively link children of child
                self._link_children(value)
            elif value:
                # list of records
                for subrecord in value:
                    rids.append(subrecord.rid)
                    # recursively link children of child
                    self._link_children(subrecord)
        # run the update in orientdb
        q = "UPDATE [{}] SET _parent = {}".format(','.join(rids), rec.rid)
        orientdb.execute(q)

    def process_ids(self, ids):
        for rec_id in ids:
            self.process_one(rec_id)

    def process_one(self, rec_id):
        obj = self.client.get('/{0}/{1}'.format(self.api_type, rec_id))
        self._process_response_record(obj)

class SpreeOrderTask(SpreeTask):
    """Downloads Order records from Spree.
    """

    def _process_response_record(self, obj):
        rec = SpreeRecord.factory(self.client, self.record_type, obj)
        with ErrorLogging(self.task_id, rec):
            rec.throw_at_orient()
            self._link_children(rec)

            current_shipments = list(map(lambda s: s['id'], obj.get('shipments', [])))

            q = "SELECT id, rid FROM {}Shipment WHERE order_id = '{}'".format(self.client.name, obj['number'])
            old_shipments = orientdb.execute(q)

            for shipment in old_shipments :
                if shipment.get('id') not in current_shipments :
                    print("Soft Delete Shipment {}".format(shipment.get('number')))
                    del_q = 'UPDATE {}Shipment SET _deleted_at = date() WHERE id = {}'.format(self.client.name, shipment.get('id'))
                    orientdb.execute(del_q)

            print("Saved {} {} as {} {}".format(
                self.record_type, obj.get('id'), rec.classname, rec.rid))


class SpreeStockItemTask(SpreeTask):
    """Downloads StockItem records from Spree.
    """

    def get_all(self, args={}):
        locations = orientdb.get('{0}{1}'.format(self.client.name, 'StockLocation'))
        for location in locations:
            endpoint = self.api_type.format(location = location.get('id'))
            self._get_all_in_namespace(endpoint, args)

    def _get_all_in_namespace(self, endpoint, args={}):
        response = None
        if 'page' not in args or not args['page']:
            args['page'] = 1

        while response is None or args['page'] <= response['pages']:
            response = self.client.get('/{0}'.format(endpoint), args)
            self._process_search_response(response, 'stock_items')
            args['page'] += 1


class ToSpreeTask(tasks.ReflectTask):
    """Checks for updates to records an pushes them into spree.
    """
    link_field = 'id'

    def __init__(self,
            from_class,
            conversion,
            client, to_type,
            where=None,
            with_reflection=None):

        super(ToSpreeTask, self).__init__(
            from_class,
            (client.name, to_type),
            where=where,
            with_reflection=with_reflection)

        self.client = client
        self.conversion = conversion

    def _process_response_record(self, obj):
        """Uploads the record to spree.
        """
        # create the record in spree
        data = self.conversion(obj)
        is_update = data.get('id', False)
        spree_result = client.save(data)

        # save the spree response to orient and link it
        spree_rec = SpreeRecord.factory(spree_result)
        self._save_response(obj, spree_rec, is_update, request=data)


class ToSpreeActionTask(tasks.ReflectTask):
    """Checks for updates to records an triggers endpoints in spree.
    """

    def __init__(self,
            from_class,
            client, to_type,
            url_format,
            conversion=None,
            where=None,
            with_reflection=True):

        super(ToSpreeActionTask, self).__init__(
            from_class,
            (client.name, to_type),
            where=where,
            with_reflection=with_reflection)

        self.client = client
        self.url_format = url_format
        self.conversion = conversion

    def _process_response_record(self, obj):
        """Hits a spree endpoint for the result
        """
        # create the record in spree
        if self.conversion:
            data = self.conversion(obj)
        else:
            data = None

        is_update = obj.get('_linked_to', '') != ''

        spree_result = self._to_spree(obj, data)

        # save the spree response to orient and link it
        spree_rec = SpreeRecord.factory(self.client, self._get_factory_type(), spree_result)
        self._save_response(obj, spree_rec, is_update, request=data)

    def _get_factory_type(self):
        return self.to_type

    def _to_spree(self, obj, data=None):
        url = self._get_url(obj, data)
        return self.client.put(url, data=data)

    def _get_url(self, obj, data=None):
        params = self._get_url_params(obj, data)
        return self.url_format.format(*params)

    def _get_url_params(self, obj, data=None):
        return (obj['_linked_to'], )


class CaptureSpreeInvoicesTask(ToSpreeActionTask):
    """Captures all "checkout" invoice payments on an order.
    """

    def __init__(self,
            from_class,
            client,
            to_type='Order',
            url_format='/orders/{}/payments/{}/capture',
            conversion=None,
            where=None,
            with_reflection=True):

        super(CaptureSpreeInvoicesTask, self).__init__(
            from_class=from_class,
            client=client,
            to_type=to_type,
            url_format=url_format,
            where=where,
            with_reflection=with_reflection)

    def _process_response_record(self, obj):
        order = obj['_linked_to']
        # capture all pending payments
        for payment in order['payments']:
            # don't capture credit cards
            if payment['state'] == 'checkout' and payment.get('source_type') != 'Spree::CreditCard':
                order['_payment'] = payment
                super(CaptureSpreeInvoicesTask, self)._process_response_record(obj)

    def _get_factory_type(self):
        return 'Payment'

    def _get_url_params(self, obj, data=None):
        order = obj['_linked_to']
        return (order['number'], order['_payment']['id'])


class ShipSpreeShipmentsTask(ToSpreeActionTask):
    """Updates shipments to a status of 'shipped' with tracking info
    """

    def __init__(self,
            from_class,
            client,
            to_type='Shipment',
            url_format='/shipments/{}/ship',
            conversion=None,
            where=None,
            with_reflection=True):

        super(ShipSpreeShipmentsTask, self).__init__(
            from_class=from_class,
            client=client,
            to_type=to_type,
            url_format=url_format,
            where=where,
            with_reflection=with_reflection)

    def _get_url(self, obj, data=None):
        url = self.url_format.format(self._get_shipment_id(obj, data=data))
        tracking = self._get_tracking_number(obj, data=data)

        if tracking is not None:
            url += '?shipment[tracking]={}'.format(tracking)

        return url

    def _get_shipment_id(self, obj, data=None):
        raise Exception('not implemented yet')

    def _get_tracking_number(self, obj, data=None):
        raise Exception('not implemented yet')


class UpdateSpreeStockItemTask(ToSpreeActionTask):
    """Updates shipments to a status of 'shipped' with tracking info
    """

    def __init__(self,
            from_class,
            client,
            to_type='StockItem',
            url_format='/stock_locations/{}/stock_items/{}',
            conversion=None,
            where=None,
            with_reflection=True):

        super(UpdateSpreeStockItemTask, self).__init__(
            from_class,
            client,
            to_type,
            url_format,
            conversion=conversion,
            where=where,
            with_reflection=with_reflection)

    def _get_url(self, obj, data=None):
        raise Exception('not implemented yet')

    def _to_spree(self, obj, data=None):
        url = self._get_url(obj, data)
        return self.client.put(url, None, data=data, wrap_keys='stock_item')
