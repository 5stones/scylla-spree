from distutils.core import setup

version = '1.0.0'

setup(
    name = 'scylla-spree',
    packages = ['scylla_spree'],
    version = version,

    description = 'Scylla-Spree provides the basic utilities to integrate with the standard spree API',

    #author = '',
    #author_email = '',

    url = 'https://git@gitlab.com:5stones/scylla-spree',
    download_url = 'https://gitlab.com/5stones/scylla-spree/repository/archive.tar.gz?ref=' + version,

    keywords = 'integration scylla spree',

    classifiers=[
        #'Development Status :: 4 - Beta',
        #'Development Status :: 5 - Production/Stable',

        #'Intended Audience :: Developers',
        #'Topic :: Software Development :: Build Tools',

        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],

    install_requires = [
        'scylla',
    ],
    dependency_links=[
        'git+https://gitlab.com/5stones/scylla.git',
        'requests[security]',
    ],
)
