<a name="1.3.1"></a>
## [1.3.1](https://gitlab.com/5stones/scylla-spree/compare/v1.3.0...v1.3.1) (2018-12-07)


### Bug Fixes

* **scylla_spree/*:** Add SpreeOrderTask and override Shipment sync ([0755a52](https://gitlab.com/5stones/scylla-spree/commit/0755a52))



<a name="1.3.0"></a>
# [1.3.0](https://gitlab.com/5stones/scylla-spree/compare/v1.2.2...v1.3.0) (2018-12-04)


### Features

* **UpdateSpreeStockItemTask:** Add a task to update a spree StockItem ([269d8d1](https://gitlab.com/5stones/scylla-spree/commit/269d8d1))



<a name="1.2.2"></a>
## [1.2.2](https://gitlab.com/5stones/scylla-spree/compare/v1.2.1...v1.2.2) (2018-12-04)


### Bug Fixes

* **App:** Fix issue with missing Task import ([c9b84c8](https://gitlab.com/5stones/scylla-spree/commit/c9b84c8))



<a name="1.2.1"></a>
## [1.2.1](https://gitlab.com/5stones/scylla-spree/compare/v1.2.0...v1.2.1) (2018-12-03)


### Bug Fixes

* **CaptureSpreeInvoicesTask:** Fix issue with missing method parameter causing exception ([e00be25](https://gitlab.com/5stones/scylla-spree/commit/e00be25))



<a name="1.2.0"></a>
# [1.2.0](https://gitlab.com/5stones/scylla-spree/compare/v1.1.0...v1.2.0) (2018-11-30)


### Bug Fixes

* **setup.py:** Fix typo in dependency url ([92e2616](https://gitlab.com/5stones/scylla-spree/commit/92e2616))
* **ToSpreeActionTask:** Fix hardcoded to_class type on factory ([89d9ebd](https://gitlab.com/5stones/scylla-spree/commit/89d9ebd))


### Features

* **app, SpreeStockItemTask:** Add the ability to download StockLocations & StockItems ([f6018cf](https://gitlab.com/5stones/scylla-spree/commit/f6018cf))



<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/5stones/scylla-spree/compare/v1.0.0...v1.1.0) (2018-10-23)


### Features

* **ShipSpreeShipmentsTask:** Add task to update shipment status to shipped and add tracking informa ([d81f974](https://gitlab.com/5stones/scylla-spree/commit/d81f974))



<a name="1.0.0"></a>
# [1.0.0](https://gitlab.com/5stones/scylla-spree/compare/e0f6c63...v1.0.0) (2018-09-27)


### Bug Fixes

* **ipre, elan:** get order import working ([e0f6c63](https://gitlab.com/5stones/scylla-spree/commit/e0f6c63))
* **records:** fix 'effective_date' bug ([82559b0](https://gitlab.com/5stones/scylla-spree/commit/82559b0))
* **records:** fix admin url for orders ([4fa50a3](https://gitlab.com/5stones/scylla-spree/commit/4fa50a3))
* **records:** fix determining the order's ship date ([8921d95](https://gitlab.com/5stones/scylla-spree/commit/8921d95))
* **records:** fix discount rate to be a positive 0-100 ([125a6ee](https://gitlab.com/5stones/scylla-spree/commit/125a6ee))
* **records:** fix error caused by missing skus ([bab42c0](https://gitlab.com/5stones/scylla-spree/commit/bab42c0))
* **records:** fix instanciating of subrecords and _parent ([e346226](https://gitlab.com/5stones/scylla-spree/commit/e346226))
* **records:** fix master variant field of products to be a link ([0a7b77e](https://gitlab.com/5stones/scylla-spree/commit/0a7b77e))
* **records:** fix type of shipped_at ([91075d6](https://gitlab.com/5stones/scylla-spree/commit/91075d6))
* **records:** set variants as a subrecord on products ([5d5bfe6](https://gitlab.com/5stones/scylla-spree/commit/5d5bfe6))
* **SpreeAdjustableMixin:** fix issue with property methods ([59147dc](https://gitlab.com/5stones/scylla-spree/commit/59147dc))
* **SpreeAdjustment:** fixed issues with lambda functions not evaluating correctly ([a0d98ae](https://gitlab.com/5stones/scylla-spree/commit/a0d98ae))
* **SpreeVariant:** fix issue with full_name not formating in unicode ([af1d12b](https://gitlab.com/5stones/scylla-spree/commit/af1d12b))
* **tasks:** add _parent to children of children, not just direct subrecords ([60158e8](https://gitlab.com/5stones/scylla-spree/commit/60158e8))
* **tasks:** fix removing errors on successful records ([70b8110](https://gitlab.com/5stones/scylla-spree/commit/70b8110))
* **vbf:** correctly format product properties string ([0cc1e00](https://gitlab.com/5stones/scylla-spree/commit/0cc1e00))


### Features

* **setup.py, setup.cfg, README.md, LICENSE:** Add necessary files for public access ([e8b6dad](https://gitlab.com/5stones/scylla-spree/commit/e8b6dad))
* **ship:** Throw error when attempting to ship a pending order ([022f992](https://gitlab.com/5stones/scylla-spree/commit/022f992))
* **SpreeAdjustment:** seperate markups and discounts ([f171442](https://gitlab.com/5stones/scylla-spree/commit/f171442))
* **tasks:** add Refunds to a subrecord of Payments ([5d487d5](https://gitlab.com/5stones/scylla-spree/commit/5d487d5)), closes [#1](https://gitlab.com/5stones/scylla-spree/issues/1)
* **tasks:** create task to capture invoices paid in another system ([623462c](https://gitlab.com/5stones/scylla-spree/commit/623462c))
* **vbf:** update for vbf order sync ([a05f63c](https://gitlab.com/5stones/scylla-spree/commit/a05f63c))



